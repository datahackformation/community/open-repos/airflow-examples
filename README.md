![](img/DataHackLogo.png)

# Ejemplo Airflow by DataHack Formation

Compartimos con toda la comunidad un ejemplo de un workflow en Airflow desarrollado en el programa de [Big Data Specialist](https://datahacks.ai/big-data-specialist/).

## Installation

Le pedimos revise la [guía de Airflow](https://airflow.apache.org/docs/stable/start.html) para poder instalarlo en su computadora local o virtual machine.

```bash
# airflow needs a home, ~/airflow is the default,
# but you can lay foundation somewhere else if you prefer
# (optional)
export AIRFLOW_HOME=~/airflow

# install from pypi using pip
pip install apache-airflow

# initialize the database
airflow initdb

# start the web server, default port is 8080
airflow webserver -p 8080

# start the scheduler
airflow scheduler

# visit localhost:8080 in the browser and enable the example dag in the home page
```

## Usage

```python
from datetime import timedelta, datetime
from airflow import models
from airflow.operators.bash_operator import BashOperator
#Continual con el código del archivo .py
```

## Contributing
El código se comparte como está con toda la comunidad ☺️

## License
[MIT](https://choosealicense.com/licenses/mit/)